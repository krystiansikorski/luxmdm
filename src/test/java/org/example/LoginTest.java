package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

  public class LoginTest extends SetupJava {


    @Test
      public void runTest() {
            driver.manage().window().maximize();
            driver.get(url);

            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

            By userLogin = By.xpath("//input[@id='login']");
            WebElement findUserLogin = driver.findElement(userLogin);
            findUserLogin.sendKeys(login);

            By userPassword = By.xpath("//input[@id='password']");
            WebElement findUserPassword = driver.findElement(userPassword);
            findUserPassword.sendKeys(password);

            By loginButton = By.xpath("//button[@class='p-element custom-button login-button p-button p-component']");
            WebElement findLoginButton = driver.findElement(loginButton);
            findLoginButton.click();
    }
}

//