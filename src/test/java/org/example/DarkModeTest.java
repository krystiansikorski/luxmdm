package org.example;

import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class DarkModeTest extends SetupJava {

    @Before
    public void setup() {
        LoginTest loginTest = new LoginTest();
        loginTest.runTest();
    }
    @Test
    public void runTest() {

        By layoutSidebar = By.cssSelector(".layout-sidebar");
        WebElement findSidebar = driver.findElement(layoutSidebar);

        Actions action = new Actions(driver);
        action.moveToElement(findSidebar).perform();

        By darkMode = By.xpath("//span[@class='dark-mode']");
        WebElement findDarkMode = driver.findElement(darkMode);
        findDarkMode.click();
    }
}
//