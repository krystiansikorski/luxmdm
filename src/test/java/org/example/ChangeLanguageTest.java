package org.example;

import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class ChangeLanguageTest extends SetupJava {

    @Before
    public void setup() {
        LoginTest loginTest = new LoginTest();
        loginTest.runTest();
    }
    @Test
    public void runTest() throws InterruptedException {

        By layoutSidebar = By.cssSelector(".layout-sidebar");
        WebElement findSidebar = driver.findElement(layoutSidebar);

        Actions action = new Actions(driver);
        action.moveToElement(findSidebar).perform();

        By changeLanguage = By.xpath("//span[@class='lang-and-mode-span']");
        WebElement findChangeLanguage = driver.findElement(changeLanguage);
        Thread.sleep(1500);
        findChangeLanguage.click();
    }
}