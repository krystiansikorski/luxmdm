package org.example;

import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class AddMeasuringPointTest extends SetupJava{

    @Before
    public void setup() {
        LoginTest loginTest = new LoginTest();
        loginTest.runTest();

    }
    @Test
    public void runTest() {

        By layoutSidebar = By.cssSelector(".layout-sidebar");
        WebElement findSidebar = driver.findElement(layoutSidebar);

        Actions action = new Actions(driver);
        action.moveToElement(findSidebar).perform();

        By measuringPoints = By.xpath("//span[@class='layout-menuitem-text ng-tns-c6-8']");
        WebElement findMeasuringPoints = driver.findElement(measuringPoints);
        findMeasuringPoints.click();

        By measuringPointsList = By.xpath("//span[@class='layout-menuitem-text ng-tns-c6-11']");
        WebElement findmeasuringPointsList = driver.findElement(measuringPointsList);
        findmeasuringPointsList.click();

        action.moveByOffset(500,10).perform();

        By addMeasuringPoint = By.xpath("/html/body/app-root/app-layout/div/div[2]/div/app-measuring-points/div/div/div[3]/p-table/div/div[1]/div/p-button[1]/button");
        WebElement findAddMeasuringPoint = driver.findElement(addMeasuringPoint);
        findAddMeasuringPoint.click();
    }
}