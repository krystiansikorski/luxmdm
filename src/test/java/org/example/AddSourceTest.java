package org.example;

import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class AddSourceTest extends SetupJava {

    @Before
    public void setup() {
        LoginTest loginTest = new LoginTest();
        loginTest.runTest();
    }
    @Test
    public void runTest() {

        By layoutSidebar = By.cssSelector(".layout-sidebar");
        WebElement findSidebar = driver.findElement(layoutSidebar);

        Actions action = new Actions(driver);
        action.moveToElement(findSidebar).perform();

        By sourceIcon = By.xpath("//span[@class='layout-menuitem-text ng-tns-c6-7']");
        WebElement findSourceIcon = driver.findElement(sourceIcon);
        findSourceIcon.click();

        By sourceList = By.xpath("//span[@class='layout-menuitem-text ng-tns-c6-10']");
        WebElement findSourceList = driver.findElement(sourceList);
        findSourceList.click();

        action.moveByOffset(500,10).perform();

        By addSource = By.xpath("//*[@id='addSource']/button");
        WebElement findAddSource = driver.findElement(addSource);
        findAddSource.click();

        By sourceName = By.xpath("//input[@id='name']");
        WebElement findSourceName = driver.findElement(sourceName);
        findSourceName.sendKeys("test");

        By dateFrom = By.xpath("//*[@id='tempDateFrom']/span/input");
        WebElement findDateFrom = driver.findElement(dateFrom);
        findDateFrom.sendKeys("2023-07-27");

        action.moveByOffset(500,10).click().perform();

        By dateTo = By.xpath("//*[@id='tempDateTo']/span/input");
        WebElement findDateTo = driver.findElement(dateTo);
        findDateTo.sendKeys("2023-07-30");


        By addDate = By.xpath("/html/body/app-root/app-layout/div/div[2]/div/app-sources-list/div/div/app-sources-modal/form/p-dialog/div/div/div[4]/button[2]");
        WebElement findAddDate = driver.findElement(addDate);
        findAddDate.click();
    }
}